import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  
  const { user, setUser } = useContext(UserContext);


  useEffect(() => {
    if((email !== '' && password !== '')) {
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password]);

  function loginUser(e) {
    e.preventDefault();

    // localStorage.setItem allows us to save a key/value pair to localStorage
    // Syntax: (key, value)

    // ACTIVITY:
    /*
    Create a fetch request inside this function to allow users to log in.
    Log in the console the response
    */

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
    })

    // localStorage.setItem('email',email);
    
    // setUser({
    //   email: email
    // });
    // //

    // setEmail("");
    // setPassword("");
    // alert('Login successful!');
  }

  return (
      (user.email !== null) ?
      <Redirect to="/"/>
      :
      <Form onSubmit={e => loginUser(e)}>
        <Form.Group controlId="userEmail">
          <Form.Label>Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder = "Enter email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>

        <Form.Group controlId="password">
          <Form.Label className="mt-3">Password</Form.Label>
          <Form.Control
            type="password"
            placeholder = "Enter Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        {isActive ?
        <Button className="mt-3" variant="primary" type="submit" id="submitBtn">
          Login
        </Button>
        :
        <Button className="mt-3" variant="primary" id="submitBtn" disabled>
          Login
        </Button>
      }

      </Form>

  );

}