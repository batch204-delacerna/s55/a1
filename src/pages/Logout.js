import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Logout(){
	
	// Get the setUser setter from App.js
	const { setUser } = useContext(UserContext);

	// clear localStorage
	localStorage.clear();

	// when the component mounts/page loads, set the user to null
	useEffect(() => {
		setUser({
			email: null
		});
	});

	return(
		// Redirects the user
		<Redirect to="/login"/>
	)
}