import { useState, useEffect } from 'react';
import { Button, Row, Col } from 'react-bootstrap';

export default function Calculator(){

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1 >Calculator</h1>
				
				<Button variant="primary" className="m-1">Add</Button>
				<Button variant="primary" className="m-1">Subtract</Button>
				<Button variant="primary" className="m-1">Multiply</Button>
				<Button variant="primary" className="m-1">Divide</Button>
				<Button variant="primary" className="m-1">Reset</Button>
			</Col>
		</Row>

	)

}