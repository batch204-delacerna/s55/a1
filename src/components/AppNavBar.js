// Long Method
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

// Short Method
import { useContext } from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){

	// A context object such as our UserContext can be "opened" with React's useContext hook
	const { user } = useContext(UserContext);

	return(
		<Navbar bg="secondary" expand="lg">
		      <Container>
		        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		        {/*
		        	mr -> me
					ml -> ms
		        */}
		          <Nav className="ms-auto">
		            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
		            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

		            {(user.email !== null) ?
		            	<Link className="nav-link" to="/logout">Logout</Link>
		            	:
		            	<>
		            		<Link className="nav-link" to="/login">Login</Link>
		            		<Link className="nav-link" to="/register">Register</Link>
		            	</>
		            }

		            <Nav.Link as={NavLink} to="/calculator">Calculator</Nav.Link>
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
	);
}